//
//  Actor.m
//  jSONRT
//
//  Created by Morten Ditlevsen on 04/10/14.
//  Copyright (c) 2014 Kaj Schermer Didriksen. All rights reserved.
//

#import "Actor.h"
#import "Character.h"
#import "Movie.h"


@implementation Actor

@dynamic name;
@dynamic actorID;
@dynamic movies;
@dynamic characters;

@end
