//
//  Character.h
//  jSONRT
//
//  Created by Morten Ditlevsen on 04/10/14.
//  Copyright (c) 2014 Kaj Schermer Didriksen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Character : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSManagedObject *movie;
@property (nonatomic, retain) NSManagedObject *actor;

@end
