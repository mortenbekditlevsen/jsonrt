//
//  Character.m
//  jSONRT
//
//  Created by Morten Ditlevsen on 04/10/14.
//  Copyright (c) 2014 Kaj Schermer Didriksen. All rights reserved.
//

#import "Character.h"


@implementation Character

@dynamic name;
@dynamic movie;
@dynamic actor;

@end
