//
//  KSDCastListViewController.h
//  jSONRT
//
//  Created by Kaj Schermer Didriksen on 01/10/14.
//  Copyright (c) 2014 Kaj Schermer Didriksen. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Movie;

@interface KSDCastListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic) Movie *movie;
@property (nonatomic, strong) NSArray *sortedActors;

@end
