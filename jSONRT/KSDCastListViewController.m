//
//  KSDCastListViewController.m
//  jSONRT
//
//  Created by Kaj Schermer Didriksen on 01/10/14.
//  Copyright (c) 2014 Kaj Schermer Didriksen. All rights reserved.
//

#import "KSDCastListViewController.h"
#import "Actor.h"
#import "Movie.h"
#import "Character.h"

@interface KSDCastListViewController ()
@property (weak, nonatomic) IBOutlet UITableView *movieCastList;
@end

@implementation KSDCastListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey: @"name" ascending: YES];
    self.sortedActors = [self.movie.actors sortedArrayUsingDescriptors: @[sortDescriptor]];
                         
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"castCell" forIndexPath:indexPath];
    
    Actor *cellItem = [self.sortedActors objectAtIndex:indexPath.row];
    cell.textLabel.text = cellItem.name;
    
    NSMutableArray *characterNames = [NSMutableArray arrayWithCapacity: cellItem.characters.count];
    for (Character *character in cellItem.characters) {
        if (character.movie == self.movie) {
            [characterNames addObject: character.name];
        }
    }
    
    cell.detailTextLabel.text = [characterNames componentsJoinedByString:@"\r"];
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.movie.actors count];
}


@end
