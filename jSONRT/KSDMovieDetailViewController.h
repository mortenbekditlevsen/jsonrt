//
//  KSDMovieDetailViewController.h
//  jSONRT
//
//  Created by Kaj Schermer Didriksen on 23/09/14.
//  Copyright (c) 2014 Kaj Schermer Didriksen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KSDCastListViewController.h"

@class Movie;

@interface KSDMovieDetailViewController : UIViewController

@property (nonatomic, strong) Movie *theMovie;

@end
