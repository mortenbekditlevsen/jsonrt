//
//  KSDMovieDetailViewController.m
//  jSONRT
//
//  Created by Kaj Schermer Didriksen on 23/09/14.
//  Copyright (c) 2014 Kaj Schermer Didriksen. All rights reserved.
//

#import "KSDMovieDetailViewController.h"
#import "Movie.h"

@interface KSDMovieDetailViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *moviePoster;
@property (weak, nonatomic) IBOutlet UITextView *movieDescription;
@property (weak, nonatomic) IBOutlet UILabel *movieRating;

@end

@implementation KSDMovieDetailViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    self.moviePoster.image = [self.theMovie createMoviePoster];
    self.movieDescription.text = self.theMovie.movieDescription;
    self.movieRating.text = [NSString stringWithFormat:@"%@",self.theMovie.rating];
    self.title = self.theMovie.title;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"movieCast"]){
        
        KSDCastListViewController *controller = segue.destinationViewController;
        controller.movie = self.theMovie;
        
    }
    
}




@end
