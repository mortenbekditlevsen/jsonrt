//
//  KSDMovieList.h
//  jSONRT
//
//  Created by Kaj Schermer Didriksen on 16/09/14.
//  Copyright (c) 2014 Kaj Schermer Didriksen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface KSDMovieList : NSObject

@property (nonatomic) NSString *nameOfMovieList;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *apiKey;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, assign) int limit;


-(instancetype)initWithURL:(NSString *)url
                    apiKey:(NSString *)apiKey
               returnLimit:(int)limit
               countryCode:(NSString *)country;


-(void) fetchMoviesWithCompletion: (void(^)(NSArray *movieList)) completion;
-(NSArray*) fetchCachedMoviesWithManagedObjectContext: (NSManagedObjectContext*) context;

@end
