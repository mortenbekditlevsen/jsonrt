//
//  KSDMovieList.m
//  jSONRT
//
//  Created by Kaj Schermer Didriksen on 16/09/14.
//  Copyright (c) 2014 Kaj Schermer Didriksen. All rights reserved.
//

#import "KSDMovieList.h"
#import "Movie.h"
#import "Actor.h"
#import "Character.h"
#import "KSDAppDelegate.h"

@implementation KSDMovieList

-(instancetype)initWithURL:(NSString *)url
                    apiKey:(NSString *)apiKey
               returnLimit:(int)limit
               countryCode:(NSString *)country
{
    if (self = [super init]){
        self.url = url;
        self.apiKey = apiKey;
        self.limit = limit;
        self.country = country;
    }

    return self;
}

-(void) fetchMoviesWithCompletion: (void(^)(NSArray *movieList)) completion {
    NSString * theURLString = [NSString stringWithFormat:@"%@?apikey=%@&limit=%i&country=%@", _url, _apiKey, _limit, _country];
    NSURL *theURL = [NSURL URLWithString:theURLString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL: theURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval: 10];
    
    NSManagedObjectContext *context = [((KSDAppDelegate*)[UIApplication sharedApplication].delegate) managedObjectContext];

    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response,
                                                NSData *data,
                                                NSError *error)
     {
         
         NSDictionary *responseDict = nil;
         if (data != nil) {
             
             responseDict = [NSJSONSerialization JSONObjectWithData:data
                                                            options:NSJSONReadingMutableContainers
                                                              error:&error];
         }
         if (responseDict == nil || error != nil) {
             NSLog(@"\nE R R O R:\n%@", [error localizedDescription]);
             
             if (completion) {
                 completion([self fetchCachedMoviesWithManagedObjectContext: context]);
             }
             
             return;
         }
         
         //NSLog(@"%@", response);
         
         
         /*
          Laver en dateformatter da teksten i JSON filen der viser dato er i formatet yyy-MM-dd
          Denne dateformatter kan bruges senere til at lave et "rigtig" dato objekt fra tekststrengen
          Således kan vi engang senere i listen lave yderligere en DateFormatter til at vise datoen i
          et andet format og vi kan sende datoen til notifikationsmanageren som så kan give os besked
          om at det er tid til at gi i biffen.
          */
         
         NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
         [dateFormatter setDateFormat:@"yyyy-MM-dd"];
         
         
         
         NSMutableArray *movieList = [NSMutableArray arrayWithCapacity: [[responseDict objectForKey: @"movies"] count]];
         
         for (NSDictionary *movieDict in [responseDict objectForKey:@"movies"])
         {
             NSString *movieID = movieDict[@"id"];
             Movie *movie = [self fetchMovieById: movieID inManagedObjectContext: context];
             if (movie == nil) {
                 movie = [NSEntityDescription insertNewObjectForEntityForName: @"Movie" inManagedObjectContext: context];
                 movie.title = movieDict[@"title"];
                 movie.thumbnail = movieDict[@"posters"][@"thumbnail"];
                 movie.poster = [self getCorrectedDetailURL: movieDict[@"posters"][@"detailed"]];
                 movie.year = @([movieDict[@"year"] intValue]);
                 movie.movieDescription = movieDict[@"synopsis"];
                 movie.rating = @([movieDict[@"ratings"][@"audience_score"] integerValue]);
                 movie.mpaaRating = movieDict[@"mpaa_rating"];
                 movie.releaseDate = [dateFormatter dateFromString:movieDict[@"release_dates"][@"theater"]];
                 movie.movieID = movieID;
                 
                 for (NSDictionary *castDictionary in movieDict[@"abridged_cast"]) {
                     NSString *actorId = castDictionary[@"id"];
                     Actor *actor = nil;
                     actor = [self fetchActorById: actorId inManagedObjectContext: context];
                     if (actor == nil) {
                         actor = [NSEntityDescription insertNewObjectForEntityForName: @"Actor" inManagedObjectContext: context];
                         actor.name = castDictionary[@"name"];
                         actor.actorID = actorId;
                     }
                     else {
                         NSLog(@"Actor found!");
                     }
                     [movie addActorsObject: actor];
                     
                     for (NSString *characterName in castDictionary[@"characters"]) {
                         Character *character = [NSEntityDescription insertNewObjectForEntityForName: @"Character" inManagedObjectContext: context];
                         character.name = characterName;
                         [actor addCharactersObject: character];
                         [movie addCharactersObject: character];
                     }
                 }
             }
             else {
                 NSLog(@"Movie %@ already in database", movie.title);
             }
             
             
             [movieList addObject: movie];
             
         }
         NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey: @"releaseDate" ascending: NO];
         [movieList sortUsingDescriptors: @[sortDescriptor]];
         if (completion) {
             completion(movieList);
         }
         [context save: nil];
         
         
     }];
}


-(NSString*) getCorrectedDetailURL: (NSString*) thumbnail {
    // detailed = "http://content7.flixster.com/movie/11/18/04/11180425_tmb.jpg";
    // should be "http://content7.flixster.com/movie/11/18/04/11180425_det.jpg"
    // A bug on RottenTomato forces us to correct the URL
    NSArray *splitList = [thumbnail componentsSeparatedByString:@"_tmb"];
    if ([splitList count] > 1){
        return [NSString stringWithFormat:@"%@_det%@", [splitList firstObject], [splitList lastObject]];
    }
    return thumbnail;
}

-(Actor*) fetchActorById: (NSString*) actorId inManagedObjectContext: (NSManagedObjectContext*) context {
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"Actor"];
    request.predicate = [NSPredicate predicateWithFormat: @"actorID = %@", actorId];
    NSArray *results = [context executeFetchRequest: request error: nil];
    return [results firstObject];    
}

-(Movie*) fetchMovieById: (NSString*) movieId inManagedObjectContext: (NSManagedObjectContext*) context {
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"Movie"];
    request.predicate = [NSPredicate predicateWithFormat: @"movieID = %@", movieId];
    NSArray *results = [context executeFetchRequest: request error: nil];
    return [results firstObject];
}


-(NSArray*) fetchCachedMoviesWithManagedObjectContext: (NSManagedObjectContext*) context {
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"Movie"];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey: @"releaseDate" ascending: NO];
    request.sortDescriptors = @[sortDescriptor];
    return [context executeFetchRequest: request error: nil];
}

@end
