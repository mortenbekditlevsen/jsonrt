//
//  KSDMovieListCell.h
//  jSONRT
//
//  Created by Kaj Schermer Didriksen on 01/10/14.
//  Copyright (c) 2014 Kaj Schermer Didriksen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KSDMovieListCell : UITableViewCell

@property (nonatomic) IBOutlet UIImageView *movieThumbnail;
@property (weak, nonatomic) IBOutlet UILabel *movieHeadline;
@property (nonatomic) IBOutlet UILabel *movieReleaseDate;
@property (nonatomic) IBOutlet UIImageView *movieMPAARating;

@end
