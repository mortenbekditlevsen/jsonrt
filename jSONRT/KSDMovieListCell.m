//
//  KSDMovieListCell.m
//  jSONRT
//
//  Created by Kaj Schermer Didriksen on 01/10/14.
//  Copyright (c) 2014 Kaj Schermer Didriksen. All rights reserved.
//

#import "KSDMovieListCell.h"

@implementation KSDMovieListCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
