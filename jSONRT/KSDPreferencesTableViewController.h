//
//  KSDPreferencesTableViewController.h
//  jSONRT
//
//  Created by Kaj Schermer Didriksen on 01/10/14.
//  Copyright (c) 2014 Kaj Schermer Didriksen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KSDPreferencesTableViewController : UITableViewController

@end
