//
//  KSDViewController.h
//  jSONRT
//
//  Created by Kaj Schermer Didriksen on 16/09/14.
//  Copyright (c) 2014 Kaj Schermer Didriksen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KSDMovieList.h"
#import "KSDMovieDetailViewController.h"
#import "KSDMovieListCell.h"


@interface KSDViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) NSArray *movieList;
@end
