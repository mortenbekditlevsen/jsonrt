//
//  KSDViewController.m
//  jSONRT
//
//  Created by Kaj Schermer Didriksen on 16/09/14.
//  Copyright (c) 2014 Kaj Schermer Didriksen. All rights reserved.
//

#import "KSDViewController.h"
#import "Movie.h"
#import "KSDAppDelegate.h"

@interface KSDViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation KSDViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    KSDMovieList *movieList = [[KSDMovieList alloc]initWithURL:@"http://api.rottentomatoes.com/api/public/v1.0/lists/movies/upcoming.json"
                                                        apiKey:@"f7yq9xerwnbxwfr7kyxkbjwx"
                                                   returnLimit:20
                                                   countryCode:@"DK"];
    
    NSManagedObjectContext *context = [((KSDAppDelegate*)[UIApplication sharedApplication].delegate) managedObjectContext];
    self.movieList = [movieList fetchCachedMoviesWithManagedObjectContext: context];
    
    [movieList fetchMoviesWithCompletion:^(NSArray *movieList) {
        self.movieList = movieList;
        [self.tableView reloadData];
    }];
    
}

#pragma mark - Table view data source

// Antal sections i vores tabel (Vi har kun en)
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.movieList count];
}

// Kaldes for hver celle der skal vises. Det er ikke nødvendigvis
// alle celler, men kun de celler der er synlige
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    KSDMovieListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"movieCell" forIndexPath:indexPath];
    
    //Få fat i den hentet celles model (et to do item i vores array)
    Movie *cellItem = [self.movieList objectAtIndex:indexPath.row];
    
    cell.movieHeadline.text = cellItem.title;
    
    // skal lige omformattere datoen til et mere DK format. Kunne bruge locale
    // men har her valgt at bruge et hardcoded format (dd/MM/yyyy)
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    
    cell.movieReleaseDate.text = [dateFormatter stringFromDate:cellItem.releaseDate];
    
    dateFormatter = nil;
    
    NSURL *theURL = [NSURL URLWithString:cellItem.thumbnail];
    [cellItem downloadImageFromURL:theURL
                   completionBlock:^(BOOL succeeded, UIImage *image) {
                       if (succeeded){
                           cell.movieThumbnail.image = image;
                           [cell setNeedsLayout];
                       } else {
                           cell.movieThumbnail.image = nil;
                       }
                   }];
    cell.movieMPAARating.image = [UIImage imageNamed:cellItem.mpaaRating];
    
    return cell;

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"movieDetail"]){
        
        KSDMovieDetailViewController *mc = segue.destinationViewController;
        mc.theMovie = [self.movieList objectAtIndex:[self.tableView indexPathForSelectedRow].row];
        
    }
    
}

-(IBAction)returnToMovieList:(UIStoryboardSegue *)segue
{
    
    
}


@end
