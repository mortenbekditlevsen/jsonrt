//
//  Movie.h
//  jSONRT
//
//  Created by Morten Ditlevsen on 04/10/14.
//  Copyright (c) 2014 Kaj Schermer Didriksen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Character;

@interface Movie : NSManagedObject

@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * year;
@property (nonatomic, retain) NSString * poster;
@property (nonatomic, retain) NSString * thumbnail;
@property (nonatomic, retain) NSString * movieDescription;
@property (nonatomic, retain) NSNumber * rating;
@property (nonatomic, retain) NSString * mpaaRating;
@property (nonatomic, retain) NSDate * releaseDate;
@property (nonatomic, retain) NSSet *actors;
@property (nonatomic, retain) NSSet *characters;
@property (nonatomic, retain) NSData *thumbnailImageData;
@property (nonatomic, retain) NSData *posterImageData;
@property (nonatomic, retain) NSString * movieID;


-(UIImage *)createMoviePoster;
-(void)downloadImageFromURL: (NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock;

@end

@interface Movie (CoreDataGeneratedAccessors)

- (void)addActorsObject:(NSManagedObject *)value;
- (void)removeActorsObject:(NSManagedObject *)value;
- (void)addActors:(NSSet *)values;
- (void)removeActors:(NSSet *)values;

- (void)addCharactersObject:(Character *)value;
- (void)removeCharactersObject:(Character *)value;
- (void)addCharacters:(NSSet *)values;
- (void)removeCharacters:(NSSet *)values;

@end
