//
//  Movie.m
//  jSONRT
//
//  Created by Morten Ditlevsen on 04/10/14.
//  Copyright (c) 2014 Kaj Schermer Didriksen. All rights reserved.
//

#import "Movie.h"
#import "Character.h"


@implementation Movie

@dynamic title;
@dynamic year;
@dynamic poster;
@dynamic thumbnail;
@dynamic movieDescription;
@dynamic rating;
@dynamic mpaaRating;
@dynamic releaseDate;
@dynamic actors;
@dynamic characters;
@dynamic thumbnailImageData;
@dynamic posterImageData;
@dynamic movieID;

-(UIImage *)createMoviePoster
{
    if (!self.posterImageData) {
        NSString *thePosterString = self.poster;
        NSURL *thePosterURL = [NSURL URLWithString:thePosterString];
        self.posterImageData = [NSData dataWithContentsOfURL:thePosterURL];
    }
    
    return [[UIImage alloc] initWithData: self.posterImageData];
}

-(void)downloadImageFromURL: (NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    if (self.thumbnailImageData) {
        UIImage *image = [[UIImage alloc] initWithData: self.thumbnailImageData];
        if (completionBlock) {
            completionBlock(YES, image);
        }
        return;
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data,
                                               NSError *error)
     {
         if (!error){
             self.thumbnailImageData = data;
             [self.managedObjectContext save: nil];
             
             UIImage *image = [[UIImage alloc] initWithData:data];
             completionBlock(YES, image);
             
         } else {
             completionBlock(NO, nil);
         }
     }];
}


@end
