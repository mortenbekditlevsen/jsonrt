//
//  main.m
//  jSONRT
//
//  Created by Kaj Schermer Didriksen on 16/09/14.
//  Copyright (c) 2014 Kaj Schermer Didriksen. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "KSDAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KSDAppDelegate class]));
    }
}
